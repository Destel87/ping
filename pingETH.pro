TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp

TARGET = pingETH

# deployment directives
target.path = /home/root
INSTALLS += target
